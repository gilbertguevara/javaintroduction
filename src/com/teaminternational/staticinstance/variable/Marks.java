package com.teaminternational.staticinstance.variable;

class Marks {
    //These variables are instance variables.
    //These variables are in a class and are not inside any function
    int engMarks;
    int mathsMarks;
    int phyMarks;
}

class MarksDemo {
    public static void main(String args[]) {   //first object
        Marks student1 = new Marks();
        student1.engMarks = 50;
        student1.mathsMarks = 80;
        student1.phyMarks = 90;

        //second object
        Marks student2 = new Marks();
        student2.engMarks = 80;
        student2.mathsMarks = 60;
        student2.phyMarks = 85;

        //displaying marks for first object
        System.out.println("Marks for first student:");
        System.out.println(student1.engMarks);
        System.out.println(student1.mathsMarks);
        System.out.println(student1.phyMarks);

        //displaying marks for second object
        System.out.println("Marks for second student:");
        System.out.println(student2.engMarks);
        System.out.println(student2.mathsMarks);
        System.out.println(student2.phyMarks);
    }
}
