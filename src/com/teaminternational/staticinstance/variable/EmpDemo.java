package com.teaminternational.staticinstance.variable;

class Emp {

   // static variable SALARY
   static double SALARY;
   public static String name = "Harsh";
}

public class EmpDemo
{
     public static void main(String args[]) {

      //accessing static variable without object
      Emp.SALARY = 1000;
      System.out.println(Emp.name + "'s average salary:" + Emp.SALARY);
   }

}
