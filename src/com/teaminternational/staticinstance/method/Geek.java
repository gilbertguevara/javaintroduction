package com.teaminternational.staticinstance.method;

class Geek{

    static String GEEK_NAME = "";
    String name;

    static void GEEK(String name) {
        GEEK_NAME = name;
    }

    void geek(String name){
        this.name = name;
    }
}

class Main {
    public static void main (String[] args) {
        // Accessing the static method GEEK() and
        // field by class name itself.
        Geek.GEEK("Jake Wharton");
        System.out.println(Geek.GEEK_NAME);

        // Accessing the static method GEEK() by using Object's reference.
        Geek obj = new Geek();
        obj.GEEK("John Carmack");
        System.out.println(obj.GEEK_NAME);
    }
}
