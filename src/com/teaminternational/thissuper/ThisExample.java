package com.teaminternational.thissuper;

class ThisExample {
    // instance variable
    int a = 10;

    // static variable
    static int b = 20;

    void method()
    {
        // referring current class(i.e, class ThisExample)
        // instance variable(i.e, a)
        this.a = 100;

        System.out.println(a);

        // referring current class(i.e, class ThisExample)
        // static variable(i.e, b)
        this.b = 600;

        System.out.println(b);
    }

    public static void main(String[] args)
    {
        // Uncomment this and see here you get
        // Compile Time Error since cannot use
        // 'this' in static context.
        // this.a = 700;
        new ThisExample().method();
    }
}
