package com.teaminternational.composition;

public class Main {

  public static void main(String[] args) {
    Job engineerJob = new Job("Engineer", 2500);
    Job designerJob = new Job("Designer", 3000);
    Employee employee1 = new Employee("John Doe", designerJob);
    Employee employee2 = new Employee("Jane Poe", engineerJob);

    System.out.println(employee1.toString());
    System.out.println(employee2.toString());
  }
}
