package com.teaminternational.composition;

public class Job {
    private String role;
    private long salary;
    
    Job(String role,long salary){
      this.role = role;
      this.salary = salary;
    }
        
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
    public long getSalary() {
        return salary;
    }
    public void setSalary(long salary) {
        this.salary = salary;
    }
}