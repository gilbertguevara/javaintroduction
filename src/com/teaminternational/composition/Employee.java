package com.teaminternational.composition;

// inheritance from Person
public class Employee extends Person { 

  // composition has-a relationship
  private Job job;

  Employee(String name, Job job) {
    super(name);
    this.job = job;
  }

  public String getRole() {
    return job.getRole();
  }

  public String toString() {
    return "Role:" + this.job.getRole() + ", Name:" + this.getName();  
  }

}