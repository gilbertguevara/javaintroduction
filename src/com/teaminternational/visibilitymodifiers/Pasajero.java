package com.teaminternational.visibilitymodifiers;

public class Pasajero {
    public String nombre;
    public double numeroPasaje;

    public void imprimirNumeroPasaje() {
        if (this.numeroPasaje > 5 && this.nombre == "Pedro") {
            System.out.println("El número del pasaje es mayor que 5 y es de " + this.nombre + " es: " + this.numeroPasaje);
        } else if (this.nombre == "Juan") {
            System.out.println("El número del pasaje de " + this.nombre + " es: " + this.numeroPasaje);
        } else {
            System.out.println("El número del pasaje es inválido");
        }
    }

    public static void main(String[] args) {
        Pasajero pasajero1 = new Pasajero();
        pasajero1.nombre = "Juan";

        Pasajero pasajero2 = new Pasajero();
        pasajero2.nombre = "Pedro";

        pasajero1.numeroPasaje = 5;
        pasajero2.numeroPasaje = 8;

        System.out.println(pasajero1.nombre);
        System.out.println(pasajero2.nombre);
        pasajero1.imprimirNumeroPasaje();
        pasajero2.imprimirNumeroPasaje();
    }
}
