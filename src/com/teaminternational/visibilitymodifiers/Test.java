package com.teaminternational.visibilitymodifiers;

public class Test {
    Pasajero pasajero = new Pasajero();

    public void metodo() {
        pasajero.nombre ="Juan";

        if (pasajero.nombre == "Juan") {
            System.out.println("Nombre: Juan");
        }
    }

    public static void main(String[] args) {
        Test test = new Test();
        test.metodo();
    }
}
