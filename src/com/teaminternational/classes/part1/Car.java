package com.teaminternational.classes.part1;

public class Car {

    private int doors;
    private int wheels;
    private String model;
    private String engine;
    private String color;
    private String brandName;

    public void drive() {
        AnonymousClass anonymousClass = new AnonymousClass() {
            @Override
            public void method() {  
            }
        };
    }

    class Wheel {
    }

    interface AnonymousClass {
        public void method();
    }


}
