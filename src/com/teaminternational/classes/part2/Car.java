package com.teaminternational.classes.part2;

/**
 * Created by dev on 8/3/15.
 */
public class Car {

    private int doors;
    private int wheels;
    private String branch;
    private String model;
    private String engine;
    private String colour;

    public void setModel(String model) {
        String validModel = model.toLowerCase();
        if(validModel.equals("a3") || validModel.equals("tt")) {
            this.model = model;
        } else {
            this.model = "Unknown";
        }
    }

    public String getModel() {
        return this.model;
    }
}
