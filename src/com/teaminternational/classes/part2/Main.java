package com.teaminternational.classes.part2;

public class Main {

    public static void main(String[] args) {
	    Car audi = new Car();
        Car ford = new Car();
        audi.setModel("A4");
        System.out.println("Model is " + audi.getModel());
    }
}
