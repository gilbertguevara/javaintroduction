package com.teaminternational.method.overloading;

public class Sum {

    // Overloaded sum(). This sum takes two int parameters
    public int sum(int x, int y) {
        System.out.println("sum int");
        return (x + y);
    }

    public int sum(int[] sum) {
        int total  = 0;
        for (int i = 0; i < sum.length; i++) {
            total += sum[i];
        }
        return total;
    }

    // Overloaded sum(). This sum takes three int parameters
    public int sum(int x, int y, int z) {
        System.out.println("sum int three params");
        return (x + y + z);
    }

    // Overloaded sum(). This sum takes two double parameters
    public double sum(double x, double y) {
        System.out.println("Sum double");
        return (x + y);
    }

    // Driver code
    public static void main(String args[]) {
        Sum s = new Sum();
        System.out.println(s.sum(10, 20.0));
        System.out.println(s.sum(10, 20, 30));
        System.out.println(s.sum(10.5, 20.5));
        System.out.println(s.sum(new int[]{1, 2}));
    }
}
