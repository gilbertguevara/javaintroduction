package com.teaminternational.controlflow.switchstatement.challenge2;

/**
 * Number Of Days In Month
 * <p>
 * ---
 * ** Please read this entire page before attempting to solve this coding exercise.  Additional help is available by watching the video mentioned at the bottom of the page **
 * ---
 * <p>
 * <p>
 * Write a method isLeapYear with a parameter of type int named year.
 * <p>
 * The parameter needs to be greater than or equal to 1 and less than or equal to 9999.
 * <p>
 * If the parameter is not in that range return false.
 * <p>
 * Otherwise, if it is in the valid range, calculate if the year is a leap year and return true if it is, otherwise return false.
 * <p>
 * A year is a leap year if it is divisible by 4 but not by 100, or it is divisible by 400.
 * <p>
 * Examples of input/output:
 * <p>
 * isLeapYear(-1600); should return false since parameter is not in the range (1-9999)
 * isLeapYear(1600); should return true since 1600 is leap year
 * isLeapYear(2017); should return false since 2017 is not a leap year
 * isLeapYear(2000); should return true because 2000 is a leap year
 * <p>
 * <p>
 * Write another method getDaysInMonth with two parameters month and year.  Both of type int.
 * <p>
 * If parameter month is < 1 or > 12 return -1.
 * <p>
 * <p>
 * If parameter year is < 1 or > 9999 then return -1.
 * <p>
 * This method needs to return the number of days in the month. Be careful about leap years they have 29 days in month 2 (February).
 * <p>
 * You should check if the year is a leap year using the method isLeapYear described above.
 * <p>
 * <p>
 * Examples of input/output:
 * <p>
 * getDaysInMonth(1, 2020); should return 31 since January has 31 days.
 * getDaysInMonth(2, 2020); should return 29 since February has 29 days in a leap year and 2020 is a leap year.
 * getDaysInMonth(2, 2018); should return 28 since February has 28 days if it's not a leap year and 2018 is not a leap year.
 * getDaysInMonth(-1, 2020); should return -1 since the parameter month is invalid.
 * getDaysInMonth(1, -2020); should return -1 since the parameter year is outside the range of 1 to 9999.
 * <p>
 * HINT: Use the switch statement.
 */
public class NumberOfDaysInMonth {
    public NumberOfDaysInMonth() {
        System.out.println("Constructor");
    }

    public static void main(String[] args) {
        System.out.println("-1600 is leap year?: " + isLeapYear(-1600));
        System.out.println("1600 is leap year?: " + isLeapYear(1600));
        System.out.println("2017 is leap year?: " + isLeapYear(2017));
        System.out.println("2000 is leap year?: " + isLeapYear(2000));

        System.out.println(getDaysInMonth(1, 2020));
        System.out.println(getDaysInMonth(2, 2020));
        System.out.println(getDaysInMonth(2, 2018));
        System.out.println(getDaysInMonth(-1, 2020));
        System.out.println(getDaysInMonth(1, -2020));

    }

    private static boolean isLeapYear(int year) {
        if (year < 1 || year > 9999) {
            return false;
        }
        return ((year % 4 == 0 && year % 100 != 0)
                || year % 400 == 0);
    }

    private static int getDaysInMonth(int month, int year) {
        if (month < 1 || month > 12 || year < 1 || year > 9999) {
            return -1;
        }

//        if (month == 2) {
//            if (isLeapYear(year)) {
//                return 29;
//            } else {
//                return 28;
//            }
//        } else if (month == 1 || month == 3) {
//            return 31;
//        } else {
//            return 30;
//        }

        switch (month) {
            case 2:
                return isLeapYear(year) ? 29 : 28;
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            default:
                return 30;
        }
    }
}
