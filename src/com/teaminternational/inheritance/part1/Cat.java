package com.teaminternational.inheritance.part1;

public class Cat extends Animal {
    public Cat(String name, int brain, int body, int size, int weight) {
        super(name, brain, body, size, weight);
    }
}
