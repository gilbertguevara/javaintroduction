package com.teaminternational.inheritance.part1;

public class Labrador extends Dog {
    public Labrador(String name, int size, int weight, int eyes, int legs, int tail, int teeth, String coat) {
        super(name, size, weight, eyes, legs, tail, teeth, coat);
    }
}
