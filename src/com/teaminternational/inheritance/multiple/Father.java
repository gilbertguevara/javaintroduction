package com.teaminternational.inheritance.multiple;

public interface Father {
    String getFatherSurname();
}
