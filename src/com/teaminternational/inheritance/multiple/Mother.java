package com.teaminternational.inheritance.multiple;

public interface Mother {
    default String getMotherSurname() {
        return "Perez";
    }
}
