package com.teaminternational.inheritance.multiple;

public class Son implements Father, Mother {

    public String getSurname() {
        return getFatherSurname() + " " + getMotherSurname();
    }

    @Override
    public String getFatherSurname() {
        return "Gonzalez";
    }

    public static void main(String[] args) {
       Son son = new Son();
       System.out.println(son.getSurname());
    }
}
