package com.teaminternational.inheritance.part2;

public class Main {

    public static void main(String[] args) {
	    Animal animal = new Animal("Animal", 1, 1, 5, 5);

        Dog dog = new Dog("Yorkie", 8, 20, 2, 4, 1, 20, "long silky");
        Dog dog2 = new Dog("Terrier", 8, 20, 2, 4, 1, 20, "long silky");
        System.out.println(dog);

        dog.setTeeth(19);
        System.out.println(dog);

        System.out.println(dog2);

//        dog.eat();
        dog.walk();
//        dog.run();

    }
}
