package com.teaminternational.encapsulation.part2;

// Java program implementing Singleton class 
// with getInstance() method 
class Singleton 
{ 
    // static variable single_instance of type Singleton 
    private static Singleton single_instance = null; 
  
    // variable of type String 
    private String s;

  // private constructor restricted to this class itself
  private Singleton() {
    setS("Hello I am a string part of Singleton class");
  }

  /**
   * @param s the s to set
   */
  public void setS(String s) {
    this.s = s;
  }

  public String getS() {
    return s;
  }

  // static method to create instance of Singleton class
  public static Singleton getInstance() {
    if (single_instance == null)
      single_instance = new Singleton();

    return single_instance;
  }
}

class TestSingleton {
  public static void main(String args[]) {
    // instantiating Singleton class with variable x
    Singleton x = Singleton.getInstance();

    // instantiating Singleton class with variable y
    Singleton y = Singleton.getInstance();

    // instantiating Singleton class with variable z
    Singleton z = Singleton.getInstance();

    // changing variable of instance x
    x.setS((x.getS()).toUpperCase());

    System.out.println("String from x is " + x.getS());
    System.out.println("String from y is " + y.getS());
    System.out.println("String from z is " + z.getS());
    System.out.println("\n");

    // changing variable of instance z
    z.setS((z.getS()).toLowerCase());

    System.out.println("String from x is " + x.getS());
    System.out.println("String from y is " + y.getS());
    System.out.println("String from z is " + z.getS());
    } 
} 