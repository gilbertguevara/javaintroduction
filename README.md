# Java Introduction

## OOP Part 1 - Classes, Constructors and Inheritance 

### Classes 

A class is a user defined blueprint or prototype from which objects are created.  It represents the set of properties or methods that are common to all objects of one type. In general, class declarations can include these components, in order:

    Modifiers : A class can be public or has default access (Refer this for details).
    Class name: The name should begin with a initial letter (capitalized by convention).
    Superclass(if any): The name of the class’s parent (superclass), if any, preceded by the keyword extends. A class can only extend (subclass) one parent.
    Interfaces(if any): A comma-separated list of interfaces implemented by the class, if any, preceded by the keyword implements. A class can implement more than one interface.
    Body: The class body surrounded by braces, { }.

There are various types of classes that are used in real time applications such as nested classes, anonymous classes, lambda expressions.

### Constructors
Constructors are used for initializing new objects. Fields are variables that provides the state of the class and its objects, and methods are used to implement the behavior of the class and its objects.


### Inheritance
Inheritance is an important pillar of OOP(Object Oriented Programming). It is the mechanism in java by which one class is allow to inherit the features(fields and methods) of another class.
Important terminology:

    Super Class: The class whose features are inherited is known as super class(or a base class or a parent class).
    Sub Class: The class that inherits the other class is known as sub class(or a derived class, extended class, or child class). The subclass can add its own fields and methods in addition to the superclass fields and methods.
    Reusability: Inheritance supports the concept of “reusability”, i.e. when we want to create a new class and there is already a class that includes some of the code that we want, we can derive our new class from the existing class. By doing this, we are reusing the fields and methods of the existing class.

- Single inheritance.
- Multilevel Inheritance.
- Hierarchical Inheritance.
- Multiple Inheritance.


### Reference vs Object vs Instance vs Class
It is a basic unit of Object Oriented Programming and represents the real life entities.  A typical Java program creates many objects, which as you know, interact by invoking methods. An object consists of :

    State : It is represented by attributes of an object. It also reflects the properties of an object.
    Behavior : It is represented by methods of an object. It also reflects the response of an object with other objects.
    Identity : It gives a unique name to an object and enables one object to interact with other objects.

### this vs super
super keyword is used to access methods of the parent class while this is used to access methods of the current class.

### Method Overloading vs Overriding
Overloading allows different methods to have same name, but different signatures where signature can differ by number of input parameters or type of input parameters or both. Overloading is related to compile time (or static) polymorphism.

### Static vs Instance Methods

Instance method are methods which require an object of its class to be created before it can be called. To invoke a instance method, we have to create an Object of the class in within which it defined.

    Instance method(s) belong to the Object of the class not to the class i.e. they can be called after creating the Object of the class.
    Every individual Object created from the class has its own copy of the instance method(s) of that class.
    They can be overridden since they are resolved using dynamic binding at run time.

Static methods are the methods in Java that can be called without creating an object of class. They are referenced by the class name itself or reference to the Object of that class.

    Static method(s) are associated to the class in which they reside i.e. they can be called even without creating an instance of the class i.e ClassName.methodName(args).
    They are designed with aim to be shared among all Objects created from the same class.
    Static methods can not be overridden. But can be overloaded since they are resolved using static binding by compiler at compile time.


### Static vs Instance Variables  

Instance Variables: Instance variables are non-static variables and are declared in a class outside any method, constructor or block.

    As instance variables are declared in a class, these variables are created when an object of the class is created and destroyed when the object is destroyed.
    Unlike local variables, we may use access specifiers for instance variables. If we do not specify any access specifier then the default access specifier will be used.                    	

 Static Variables: Static variables are also known as Class variables.

    These variables are declared similarly as instance variables, the difference is that static variables are declared using the static keyword within a class outside any method constructor or block.
    Unlike instance variables, we can only have one copy of a static variable per class irrespective of how many objects we create.
    Static variables are created at start of program execution and destroyed automatically when execution ends.

To access static variables, we need not to create any object of that class, we can simply access the variable as:
`ClassName.VARIABLE_NAME`.

## OOP Part 2 - Composition, Encapsulation, and Polymorphism
### Composition
Composition the design technique to implement has-a relationship in classes.

    Think about the chair example. A chair has a Seat. A chair has a back. And a chair has a set of legs. The phrase "has a" implies a relationship where the chair owns, or at minimum, uses, another object. It is this "has a" relationship which is the basis for composition.

**Composition vs Inheritance**
    
Composition means `HAS A`

Inheritance means `IS A`

    Example: Car has a Engine and Car is a Automobile

### Encapsulation

Encapsulation is defined as the wrapping up of data under a single unit. It is the mechanism that binds together code and the data it manipulates. Other way to think about encapsulation is, it is a protective shield that prevents the data from being accessed by the code outside this shield.

```
            │ Class │ Package │ Subclass │ Subclass │ World
            │       │         │(same pkg)│(diff pkg)│ 
────────────┼───────┼─────────┼──────────┼──────────┼────────
public      │   +   │    +    │    +     │     +    │   +     
────────────┼───────┼─────────┼──────────┼──────────┼────────
protected   │   +   │    +    │    +     │     +    │         
────────────┼───────┼─────────┼──────────┼──────────┼────────
no modifier │   +   │    +    │    +     │          │    
────────────┼───────┼─────────┼──────────┼──────────┼────────
private     │   +   │         │          │          │    

+ : accessible
blank : not accessible
```
### Polymorphism 

Refers to a programming language's ability to process objects differently depending on their data type or class. 

## Arrays, Java inbuilt Lists, Autoboxing and Unboxing
- Arrays
- References Types vs Value Types
- List and ArrayList
- Autoboxing and Unboxing
- LinkedList

## Inner and Abstract Classes & Interfaces
- Interfaces
- Inner classes 
- Abstract Classes 
- Interface vs Abstract Class

## Naming Conventions and Packages. static and final keywords
- Naming Conventions
- Packages
- Scope and Visibility
- Access Modifiers
- The static statement
- The final statement
- Static Initializers
- Basic Java Collections

## Regular Expressions
- Regular Expressions Introduction
- Character classes and Boundary Matchers
- Quantifiers and the Pattern and Matcher classes
- Matcher find and Group Methods
- And, Or & Not

## Debugging and Unit Testing
- Introduction to Debugging
- More on Debugging
- Field Watch Points
- Advanced Debugging
